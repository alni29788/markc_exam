from functions import Add,Concatenate
import unittest

class TestAdd(unittest.TestCase):

	def test_equal(self):
		res=Add(2,4)
		self.assertEqual(res,6)
		
	def test_not_equal(self):
		res=Add(2,2)
		self.assertNotEqual(res,6)

	def test_con_equal(self):
		res=Concatenate("Hello ", "World!")
		self.assertEqual(res, "Hello World!")
		
	
	def test_con_not_equal(self):
		res=Concatenate("Hey", "World!")
		self.assertNotEqual(res, "Hey World!")
		
		
if __name__ == '__main__':
    unittest.main()